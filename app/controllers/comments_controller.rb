class CommentsController < ApplicationController
http_basic_authenticate_with name: "login", password: "lol", only: :destroy
    def create
        @bug = Bug.find(params[:bug_id])
        @comment = @bug.comments.create(comment_params)
        redirect_to bug_path(@bug)
    end

    def destroy
        @bug = Bug.find(params[:bug_id])
        @comment = @bug.comments.find(params[:id])
        @comment.destroy
        redirect_to bug_path(@bug)
    end

    private
        def comment_params
            params.require(:comment).permit(:commenter, :body)
        end
end
