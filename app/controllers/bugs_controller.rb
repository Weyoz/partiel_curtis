class BugsController < ApplicationController
http_basic_authenticate_with name: "hello", password: "world", except: [:index]
    def index
        @bugs = Bug.all
    end

    def show
        @bug = Bug.find(params[:id])
    end

    def new
        @bug = Bug.new
    end

    def destroy
        @bug = Bug.find(params[:id])
        @bug.destroy

        redirect_to bugs_path
    end

    def edit
        @bug = Bug.find(params[:id])
    end

    def update
        @bug = Bug.find(params[:id])
        if @bug.update(bug_params)
            redirect_to @bug
        else
            render 'edit'
        end
    end

    def create
        #render plain: params[:bug].inspect
        #@bug = Bug.new(params.require(:bug).permit(:title, :text))
        @bug = Bug.new(bug_params)
        if @bug.save
            redirect_to @bug
        else
            render 'new'
        end
    end

    private
        def bug_params
            params.require(:bug).permit(:title, :text)
        end
end
